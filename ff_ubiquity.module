<?php
/**
 * @see https://wiki.mozilla.org/Labs/Ubiquity/Ubiquity_0.1_Author_Tutorial
 */

function ff_ubiquity_perm() {
  return array(
    'use ff ubiquity commands',
    'administer ff ubiquity commands',
  );
}

function ff_ubiquity_menu() {
  $items = array();
  
  $items['admin/settings/ff-ubiquity'] = array(
    'access arguments' => array('administer ff ubiquity commands'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ff_ubiquity_settings'),
    'title' => 'Ubiquity',
  );
  
  $items['admin/settings/ff-ubiquity/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  
  $items['admin/settings/ff-ubiquity/source'] = array(
    'access arguments' => array('administer ff ubiquity commands'),
    'page callback' => 'ff_ubiquity_source',
    'title' => 'Sources',
    'type' => MENU_LOCAL_TASK,
  );
  
  $items['admin/settings/ff-ubiquity/rebuild'] = array(
    'access arguments' => array('administer ff ubiquity commands'),
    'page callback' => 'ff_ubiquity_build',
    'type' => MENU_CALLBACK,
  );
  
  $items['ff-ubiquity'] = array(
    'access arguments' => array('use ff ubiquity commands'),
    'page callback' => 'ff_ubiquity_commands',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

function ff_ubiquity_settings() {
  $form = array();
  $form['ff_ubiquity_commands_prefix'] = array(
    '#title' => t('Commands prefix'),
    '#type' => 'textfield',
    '#default_value' => _ff_ubiquity_commands_prefix(),
  );
  
  $form['ff_ubiquity_custom_commands'] = array(
    '#title' => t('Custom commands'),
    '#default_value' => _ff_ubiquity_custom_commands(),
    '#type' => 'textarea',
  );
  
  $form['rebuild_commands'] = array(
    'ff_ubiquity_devel_mode' => array(
      '#title' => t('Devel mode'),
      '#default_value' => variable_get('ff_ubiquity_devel_mode', 0),
      '#type' => 'checkbox'
    ),
    'link' => array('#value' => l(t('Rebuild the commands'), 'admin/settings/ff-ubiquity/rebuild')),
    'help' => array('#value' => '<div class="description">'. t('In some occasion, you may need to rebuild the auto generated commands. To do so, click on the above link.') .'</div>'),
  );
  $form['#submit'] = array('ff_ubiquity_build');
  return system_settings_form($form);
}

function _ff_ubiquity_commands_prefix($form_state = NULL) {
  if (isset($form_state['values']['ff_ubiquity_commands_prefix'])) {
    return $form_state['values']['ff_ubiquity_commands_prefix'];
  }
  
  $site_name = str_replace(' ', '-', variable_get('site_name', 'Drupal'));
  return variable_get('ff_ubiquity_commands_prefix', $site_name);
}

function _ff_ubiquity_custom_commands($form_state = NULL) {
  if (isset($form_state['values']['ff_ubiquity_custom_commands'])) {
    return $form_state['values']['ff_ubiquity_custom_commands'];
  }
  
  return variable_get('ff_ubiquity_custom_commands', "CmdUtils.CreateCommand({
  name: \"hello-%prefix%\",
  execute: function() {
    displayMessage(\"Hello, World!\");
  }
});");
}

function ff_ubiquity_build($form = NULL, &$form_state = NULL) {
  module_load_include('inc', 'ff_ubiquity', 'includes/node');
  $commands = '';
  $prefix = _ff_ubiquity_commands_prefix($form_state);
  
  
  foreach (module_invoke_all('ff_ubiquity') as $command){
    $commands .= "\n\n". ff_ubiquity_command_render($command);
  }
  
  
  $commands = $commands ."\n\n". _ff_ubiquity_custom_commands($form_state);
  $commands = token_replace($commands, 'ubiquity', NULL, '%', '%');
  cache_set('ff_ubiquity_commands', $commands);
  
  
  // redirect to the settings page (because it's a manual rebuild)
  if (empty($form_state) && $form != 'noredirect') {
    drupal_set_message(t('The Ubiquity commands have been rebuild.'));
    drupal_goto('admin/settings/ff-ubiquity');
  }
  return $commands;
}

function ff_ubiquity_command_render($command) {
  $pieces = array();
  $pieces[] = 'homepage: "'. url('<front>', array('absolute' => TRUE)) .'"';
  $pieces[] = 'name: "'. $command['name'] .'"';
  $pieces[] = 'takes: {'. $command['takes'] .'}';
//  $pieces[] = 'modifiers: {'. $command['modifiers'] .'}';
  if (!empty($command['modifiers'])) {
    $takes = array();
    foreach ($command['modifiers'] as $key => $value) {
      $takes[] .= $key .':'. $value;
    }
    $pieces[] = 'modifiers: {'. implode(',', $takes) .'}';
  }
  $pieces[] = 'description: "'. $command['description'] .'"';
  $pieces[] = 'execute: '. $command['execute'];
  
  
  return "CmdUtils.CreateCommand({\n". implode(",\n", $pieces) ."\n});";
}

function ff_ubiquity_preprocess_page(&$vars) {
  if (user_access('use ff ubiquity commands')) {
    drupal_set_html_head('<link rel="commands" href="'. url('ff-ubiquity',array('absolute' => TRUE)) .'" name="'. t('Ubiquity commands for @site_name', array('@site_name' => variable_get('site_name', 'Drupal'))) .'" />');
  }
  $vars['head'] = drupal_get_html_head();
}

function ff_ubiquity_commands() {
  drupal_set_header('Content-type: text/javascript; charset:UTF-8');
  
  if (variable_get('ff_ubiquity_devel_mode', 0)) {
    // we pass something as arguments
    ff_ubiquity_build('noredirect');
  }
  
  $cache = cache_get('ff_ubiquity_commands');
  print $cache->data;
  die();
}

function ff_ubiquity_source() {
  $source = ff_ubiquity_build('noredirect');
  if(module_load_include('inc', 'geshifilter', 'geshifilter.pages') !== FALSE) {
    $source = geshifilter_process($source, 'javascript');
  }
  else {
    $source = '<pre class="code javascript">'. $source .'</pre>';
    $source = t('You may improve the readablity of the code by installing the <a href="">GeSHi filter module</a>.', array('@url' => 'http://drupal.org/project/geshifilter')) . $source;
  }
  return $source;
}

function ff_ubiquity_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'ubiquity') {
    $tokens['prefix'] = _ff_ubiquity_commands_prefix();
    return $tokens;
  }
}

function ff_ubiquity_token_list($type = 'ubiquity') {
  if ($type == 'ubiquity') {
    $tokens['prefix'] = t('The commands prefix');
    return $tokens;
  }
}

function ff_ubiquity_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op == 'prepare' && isset($_GET['ff-ubiquity'])) {
    $node->title = isset($_GET['title']) ? check_plain($_GET['title']) : '';
    $node->body = isset($_GET['body']) ? filter_xss($_GET['body']) : '';
  }
}