// Written by David Kent Norman
// http://deekayen.net/
// http://drupal.org/user/972
CmdUtils.makeSearchCommand({
  name: "Drupal API",
  icon: "http://api.drupal.org/misc/favicon.ico",
  description: "Searchable API reference for Drupal.",
  url: "http://api.drupal.org/",
  defaultUrl: "http://api.drupal.org/",
  postData: {"form_id": "api_search_form", "search": "{QUERY}"},
  parser: {
    maxResults: 50,
    title: "td > a"
    }
});

CmdUtils.makeSearchCommand({
  name: "Drupal groups",
  icon: "http://groups.drupal.org/misc/favicon.ico",
  description: "Search posts for Drupal groups.",
  url: "http://groups.drupal.org/search/node/{QUERY}",
  defaultUrl: "http://groups.drupal.org/search/node",
  parser: {
    maxResults: 10,
    title: "dl.search-results > dt.title > a"
    }
});

CmdUtils.makeSearchCommand({
  name: "Drupal IRC",
  icon: "http://drupal.org/misc/favicon.ico",
  description: "Find IRC nicks for Drupal users.",
  url: "http://drupal.org/search/drupalorg/{QUERY}",
  defaultUrl: "http://drupal.org/search/drupalorg",
  parser: {
    maxResults: 10,
    title: "dl.search-results.drupalorg-results > dt.title > a"
    }
});

CmdUtils.makeSearchCommand({
  name: "Drupal issues",
  icon: "http://drupal.org/misc/favicon.ico",
  description: "Find project issues on Drupal.org.",
  url: "http://drupal.org/search/issues/{QUERY}",
  defaultUrl: "http://drupal.org/search/issues",
  parser: {
    maxResults: 10,
    title: "dl.search-results.issues-results > dt.title > a"
    }
});

CmdUtils.makeSearchCommand({
  name: "Drupal modules",
  icon: "http://drupal.org/misc/favicon.ico",
  description: "Find modules for Drupal.",
  url: "http://drupal.org/search/apachesolr_search/{QUERY}?filters=type%3Aproject_project",
  defaultUrl: "http://drupal.org/search/apachesolr_search",
  parser: {
    maxResults: 10,
    title: "dl.search-results.apachesolr_search-results > dt.title > a"
    }
});

CmdUtils.makeSearchCommand({
  name: "Drupal search",
  icon: "http://drupal.org/misc/favicon.ico",
  description: "Search all the content of Drupal.org.",
  url: "http://drupal.org/search/apachesolr_search/{QUERY}",
  defaultUrl: "http://drupal.org/search/apachesolr_search",
  parser: {
    maxResults: 10,
    title: "dl.search-results.apachesolr_search-results > dt.title > a"
    }
});

CmdUtils.makeSearchCommand({
  name: "Drupal users",
  icon: "http://drupal.org/misc/favicon.ico",
  description: "Find users on Drupal.org.",
  url: "http://drupal.org/search/user_search/{QUERY}",
  defaultUrl: "http://drupal.org/search/user_search",
  parser: {
    maxResults: 10,
    title: "dl.search-results.user_search-results > dt.title > a"
    }
});
