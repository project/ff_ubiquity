<?php 

/**
 * Implementation of the hook_ff_ubiquity()
 * @see hook_ff_ubiquity()
 * @return unknown_type
 */
function node_ff_ubiquity() {
  $commands = array();
  
  $types = node_get_types('names');
  
  foreach ($types as $type => $name) {
    $type_url_correct = strtolower(str_replace(' ', '-', $type));
    $commands[] = array(
      'name' => '%prefix%-add-'. strtolower(str_replace(' ', '-', $name)),
      'description' => t('Create a @name on @site_name', array('@name' => $name, '@site_name' => variable_get('site_name', 'Drupal'))),
      'takes' => '"title":noun_arb_text',
//      'modifiers' => '"'. t('The content of the @name', array('@name' => $name)) .'":noun_arb_text',
      'modifiers' => array(
        '"body"' => 'noun_arb_text'
      ),
      'preview' => "function( pblock, title, mods ) {
  // args contains .with and .in, both of which are input objects.
  var msg = 'Replaces \"\${titleText}\" with \${bodyText}.';
  var subs = {titleText: title.text, bodyText: mods[\"body\"].text};
  pblock.innerHTML = CmdUtils.renderTemplate( msg, subs );
}",
      'execute' => "function(title, mods){
  var titleText = title.text;
  var bodyText = mods[\"body\"].text || CmdUtils.getSelection();
  var url = '". url('node/add/'. $type_url_correct, array('absolute' => TRUE,)) ."';
  url += '". (variable_get('clean_urls', 0) ? '&' : '?') ."';
  url += 'title='+encodeURIComponent(titleText);
  url += '&body='+encodeURIComponent(bodyText);
  url += '&ff-ubiquity';
  Utils.openUrlInBrowser(url);
  
}",
    );
  }
  
  return $commands;
}

